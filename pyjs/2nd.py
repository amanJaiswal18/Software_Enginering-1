import psycopg2,sys

tableName = []
column = []
def TableAttribute(conn, tbname):
    tname = tbname[0]
    
    cur = conn.cursor()
    
    cur.execute("SELECT * FROM " + tname)
    for i in cur.description:
        column.append(i[0])
    print column
    sQuery = "SELECT "
    for i in range(len(column) - 1):
         sQuery += column[i] + ", ";
    sQuery += column[len(column)-1] + " "
    sQuery += "FROM " + tname
    cur.execute(sQuery)
    lRows = cur.fetchall();
    print lRows


def TableName(conn):
    cur = conn.cursor()
    cur.execute("""SELECT table_name FROM information_schema.tables WHERE table_schema='public' """)
    rows = cur.fetchall()
    for i in rows:
        tableName.append(i[0])
    print tableName

def connect():
    try:
        conn = psycopg2.connect(database = "se1" , user = "postgres",password = "vpceb36fg")
        TableName(conn)
        TableAttribute(conn,tableName)

    except psycopg2.DatabaseError, e:
        print 'Error %s' % e    
        sys.exit(1)
    finally:
        if conn:
            conn.close()
    #return conn

if __name__=='__main__':
    connect()
