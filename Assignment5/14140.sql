CREATE TABLE types (
    typeId      TEXT PRIMARY KEY,
    type_name   TEXT NOT NULL,
    description TEXT,
    /*
    Validation: typeId should be alphanumeric string only.
    CHECK constraint is required to determine if the input string matches 
    the valid regular expression for the alphanumeric string.
    */
    CHECK (typeId ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);



/*
Validation: function to determine whether a container is already a noncontainer or Vice Verse.
Function takes two arguments: 1. string for new container/noncontainerId, 2. integer either 0 or nonZero
2.1 If chkBit is 0 then it will check if the container is nonContainer
2.2 If chkBit is 'nonZero' then it will check if the nonContainer is container.
*/
CREATE OR REPLACE FUNCTION fValidContainerChk( str varchar( 100 ), chkBit INTEGER ) RETURNS BOOLEAN AS $$
BEGIN
	IF ( chkBit ) = 0 THEN
	     /* if the count is greater than 0, it implies that container has already been defined for being a nonContainer*/
		IF ( SELECT COUNT( * ) FROM nonContainerConcreteTypes WHERE nonContainerConcreteTypeID = str ) > 0 THEN
		   RETURN FALSE;
		END IF;
		RETURN TRUE;
	END IF;
	/* if the count is greater than 0, it implies that nonContainer has already been defined as a container*/
	IF ( SELECT COUNT( * ) FROM containerConcreteTypes WHERE containerConcreteTypeID = str ) > 0 THEN
	   RETURN FALSE;
	END IF;
	RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE containerConcreteTypes (
    containerConcreteTypeID TEXT PRIMARY KEY REFERENCES types ( typeId ),
    /* Validation: Container length, width, depth and thickness should be greater than 0.*/
    containerLengthAB       INTEGER NOT NULL CHECK( containerLengthAB>0 ),
    containerLengthAD       INTEGER NOT NULL CHECK( containerLengthAD>0 ),
    containerLengthAE       INTEGER NOT NULL CHECK( containerLengthAE>0 ),
    containerThickness      INTEGER NOT NULL CHECK( containerThickness>0 ),
    /*
    Column containerRemainingVol Use: Store the remaining volume after putting a child inside the contianer.
    Note: Volume is being added automatically by the trigger, user doesn't need to put it themselves.
    */
    containerRemainingVol   INTEGER NOT NULL CHECK( containerRemainingVol >= 0 ),
    userObjectCommentGlobal TEXT,
    /* 
     Validation: If a container is not already a nonContainer.
     CHECK constraint to determine whether a container is nonContainer or not already. 
     fValidContainerChk with chkBit as '0' is Called to validate it.
     */
    CHECK( fValidContainerChk( containerConcreteTypeID, 0 ))
);

CREATE TABLE typeInheritance (
    /*
    Validation: typeId & super_typeId should be alphanumeric.
    Since the typeId is refering to types(typeId) there's no need to check whether it is alphanumeric or not
    */
    typeId       TEXT REFERENCES types( typeId ),
    super_typeId TEXT REFERENCES containerConcreteTypes( containerConcreteTypeID ),
    PRIMARY KEY( typeId, super_typeId ),
    /* 
    Validation: If type1 is not being defined as its own super type
    Check constraint to determine if typeId is not equal to super_typeId
    */
    CHECK ( typeId != super_typeId )
);


/* 
Use: Table is used to determine if the orientation is valid or not for any face/door.
Values in the table are already been added at the creation of tables 
*/
CREATE TABLE validOrientations (
    boxValidWallOrientations TEXT PRIMARY KEY /*It is one of the ABCD, BCDA, CDAB, DABC, DCBA, CBAD, BADC,  ADCB */
);


/*
Use: Store the information of all the walls defined.
Walls for every container's wall set are being reffered to wallTable(wallId)
*/
CREATE TABLE wallTable (
    wallID TEXT PRIMARY KEY,
    /*
    Validation: typeId should be alphanumeric string only.
    CHECK constraint is required to determine if the input string matches 
    the valid regular expression for the alphanumeric string.
    */
    CHECK (wallID ~ '^[A-Za-z]+[0-9a-zA-Z]*')
    
);


/*
Use: Create a face set for all six walls of the container/nonContainer and 
all the set is applied to the container/noncontainer
*/
CREATE TABLE concreteTypeFaceSet (
    concreteWallSetID TEXT     PRIMARY KEY,
    leftWallID                 TEXT REFERENCES wallTable( wallID ),
    rightWallID                TEXT REFERENCES wallTable( wallID ),
    topWallID                  TEXT REFERENCES wallTable( wallID ),
    botWallID                  TEXT REFERENCES wallTable( wallID ),
    frontWallID                TEXT REFERENCES wallTable( wallID ),
    backWallID                 TEXT REFERENCES wallTable( wallID ),


    /*
    Validation: No wall should be used more than once
    Unique key will take care of this problem in every column
    */
    
    UNIQUE ( leftWallID, rightWallID, topWallID, botWallID, frontWallID, backWallID ),

    /* 
    Validation: No wall should be used more than once for any container
    Check if each wall in the container is different
    */
    CHECK  ( leftWallID != rightWallID AND leftWallID != topWallID AND leftWallID != botWallID AND leftWallID != frontWallID AND leftWallID != backWallID ),
    CHECK  ( rightWallID != topWallID AND rightWallID != botWallID AND rightWallID != frontWallID AND rightWallID != backWallID ),
    CHECK  ( topWallID != botWallID AND topWallID != frontWallID AND topWallID != backWallID ),
    CHECK  ( botWallID != frontWallID AND botWallID != backWallID ),
    CHECK  ( frontWallID != backWallID ),

    /*
    Validation: concreteWallSetId should be alphanumerice
    Check if the concreteWallSetId matches the regex String.
    */
    CHECK (concreteWallSetId ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);

CREATE OR REPLACE FUNCTION fValidContainerFaceChk( str varchar( 100 ), chkBit INTEGER ) RETURNS BOOLEAN AS $$
BEGIN
	IF ( chkBit ) = 0 then
		IF ( SELECT COUNT( * ) FROM nonContainerConcreteTypeFaces WHERE nonContainerConcreteWallSetID = str ) > 0 THEN
		   RETURN FALSE;
		END IF;
		RETURN TRUE;
	END IF;
	IF ( SELECT COUNT( * ) FROM containerConcreteTypeFaces WHERE containerConcreteWallSetID = str ) > 0 THEN
	   RETURN FALSE;
	END IF;
	RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE containerConcreteTypeFaces (
    containerConcreteWallSetID TEXT REFERENCES concreteTypeFaceSet( concreteWallSetID ),
    containerConcreteTypeID    TEXT REFERENCES types( typeId ),
    wallOrientation            TEXT REFERENCES validOrientations( boxValidWallOrientations ),
    colorRed                   INTEGER CHECK ( colorRed >= 0 AND colorRed <= 255 ),
    colorGreen                 INTEGER CHECK ( colorGreen >= 0  AND colorGreen <= 255 ),
    colorBlue                  INTEGER CHECK ( colorBlue >= 0 AND colorBlue <= 255 ),
    PRIMARY KEY( containerConcreteWallSetID, containerConcreteTypeID ),
    CHECK( fValidContainerFaceChk( containerConcreteWallSetID, 0 ) )    
    /*one Wall can not belong to other object or the same object for the another wall*/
);


CREATE TABLE containerConcreteTypeFaceDoors (
    containerConcreteDoorID TEXT PRIMARY KEY,
    containerConcreteWallID TEXT REFERENCES wallTable( wallID ),
    doorWidthAB             INTEGER NOT NULL,
    doorLengthAD            INTEGER NOT NULL,
    doorDisplacementAB      INTEGER NOT NULL,
    doorDisplacementAD      INTEGER NOT NULL,
    doorAlignmentAB         TEXT NOT NULL REFERENCES validOrientations( boxValidWallOrientations ),
    doorAlignmentAD         TEXT NOT NULL REFERENCES validOrientations( boxValidWallOrientations ),
    CHECK (containerConcreteDoorId ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);


CREATE TABLE objects (
    objectID   TEXT PRIMARY KEY,
    objectType TEXT REFERENCES types( typeId ),
    /*
    Validation: objectID should be alphanumerice
    Check if the objectID matches the regex String.
    */
    CHECK (objectId ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);

CREATE TABLE physicalEntities (
    physicalEntityID TEXT PRIMARY KEY,
    entity_name      TEXT NOT NULL,
    CHECK (physicalEntityId ~ '^[A-Za-z]+[0-9a-zA-Z]*')
    
);

CREATE TABLE propertyTypes (
    propertyType     TEXT PRIMARY KEY, /* only from a list - date, string, bool, number, list of string, bool, number*/
    physicalEntityID TEXT REFERENCES physicalEntities( physicalEntityID ), 
    userDescription  TEXT,
    CHECK (propertyType ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);


CREATE TABLE properties (
    propertyID      TEXT PRIMARY KEY,
    propertyType    TEXT REFERENCES propertyTypes( propertyType ),
    userDescription TEXT,
    CHECK (propertyId ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);

CREATE TABLE Unit_List (
    unit_id               TEXT PRIMARY KEY,
    unit_name             TEXT,
    unit_abbr             TEXT,
    physicalEntityID      TEXT REFERENCES physicalEntities( physicalEntityID ), 
    multiplication_factor FLOAT,
    CHECK (unit_id ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);

CREATE TABLE measurementUnits (
    measurementUnitId   TEXT PRIMARY KEY,
    measurementUnitType TEXT REFERENCES Unit_List( unit_id )
    CHECK (measurementUnitId ~ '^[A-Za-z]+[0-9a-zA-Z]*')
);

CREATE TABLE containerConcreteTypeProperties (
    containerConcreteTypeID TEXT REFERENCES containerConcreteTypes (containerConcreteTypeID),
    propertyID              TEXT REFERENCES properties(propertyID),
    propertyValue           TEXT,
    measurementUnits        TEXT REFERENCES measurementUnits(measurementUnitID),
    userCommentGlobal       TEXT,
    PRIMARY KEY( containerConcreteTypeID, propertyID )
);

CREATE TABLE containerObjects (
    objectID                TEXT REFERENCES objects( objectID ),
    containerConcreteTypeId TEXT REFERENCES containerConcreteTypes ( containerConcreteTypeId ),
    userLabel               TEXT,
    userObjectCommentLocal  TEXT,
    PRIMARY KEY( objectID )
);

CREATE TABLE ContainerObjectProperties (
    objectID         TEXT REFERENCES containerObjects ( objectID ),
    userAssignedID   TEXT NOT NULL,
    propertyID       TEXT REFERENCES properties( propertyID ), 
    propertyValue    TEXT NOT NULL,
    userCommentLocal TEXT,
    userLabel        TEXT NOT NULL,
    PRIMARY KEY( objectID, propertyID )
);

CREATE TABLE nonContainerConcreteTypes (
    nonContainerConcreteTypeID TEXT PRIMARY KEY REFERENCES types( typeId ),
    nonContainerLengthAB       INTEGER NOT NULL,
    nonContainerLengthAD       INTEGER NOT NULL,
    nonContainerLengthAE       INTEGER NOT NULL,
    userObjectCommentGlobal    TEXT,
    CHECK( fValidContainerChk( nonContainerConcreteTypeID, 1 ) )	
);

CREATE TABLE nonContainerConcreteTypeFaces (
    nonContainerConcreteWallSetID TEXT REFERENCES concreteTypeFaceSet( concreteWallSetID ),
    nonContainerConcreteTypeID    TEXT REFERENCES nonContainerConcreteTypes( nonContainerConcreteTypeID ),
    wallOrientation               TEXT REFERENCES validOrientations( boxValidWallOrientations ),
    PRIMARY KEY( nonContainerConcreteWallSetID, nonContainerConcreteTypeID ),
    CHECK( fValidContainerFaceChk( nonContainerConcreteWallSetID, 0 ) )
);

CREATE TABLE nonContainerObjects (
    objectID                   TEXT REFERENCES objects( objectID ),
    nonContainerConcreteTypeId TEXT REFERENCES nonContainerConcreteTypes ( nonContainerConcreteTypeId ),
    userLabel                  TEXT,
    userObjectCommentLocal     TEXT,
    PRIMARY KEY( objectID )
);

CREATE TABLE nonContainerObjectProperties (
    objectID         TEXT REFERENCES nonContainerObjects ( objectID ),
    userAssignedID   TEXT NOT NULL,
    propertyID       TEXT REFERENCES properties( propertyID ), 
    propertyValue    TEXT NOT NULL,
    userCommentLocal TEXT,
    userLabel        TEXT NOT NULL,
    PRIMARY KEY( objectID, propertyID )
);


--Check if the child is not already the parent of the same child.
CREATE OR REPLACE FUNCTION fValidChildParentPair( str varchar( 100 ), str1 varchar( 100 ) ) RETURNS BOOLEAN AS $$
BEGIN
	IF ( SELECT COUNT( * ) FROM childrenList WHERE objectID = str AND parentObjectID = str1 ) > 0 THEN
	   RETURN FALSE;
	END IF;
	RETURN TRUE;
END;
$$ LANGUAGE plpgsql;


CREATE TABLE childrenList (
    parentObjectID         TEXT REFERENCES containerObjects( objectID ),
    objectID               TEXT REFERENCES objects( objectID ),
    displacementAB         INTEGER NOT NULL,
    displacementAD         INTEGER NOT NULL,
    displacementAE         INTEGER NOT NULL,
    childFrontFacingParent TEXT    NOT NULL,
    childTopFacingParent   TEXT    NOT NULL
    CHECK( objectID != parentObjectID )
    CHECK( fValidChildParentPair( parentObjectID, objectID ) )
);

CREATE TABLE compulsoryProperties ( 
       propertyID TEXT REFERENCES properties( propertyID )
    /*If property is there it is compulsory else its not.*/
);

CREATE TABLE immutableProperties ( 
    propertyID TEXT REFERENCES properties ( propertyID )
    /*If property is there it is immutable else its not.*/
);


-- ALTER TABLE containerConcreteTypes
-- ADD CHECK (containerConcreteTypeID != nonContainerConcreteTypes.nonContainerConcreteTypeID);


/*Insertion into tables for validations*/

INSERT INTO validOrientations VALUES( 'ABCD' ), ( 'BCDA' ), ( 'CDAB' ), ( 'DABC' ), ( 'DCBA' ), ( 'CBAD' ), ( 'BADC' ), ( 'ADCB' );



CREATE OR REPLACE FUNCTION fSubTypeSetChk() RETURNS TRIGGER AS $tSubTypeSetChk$
DECLARE
	rec record;
	sTypeId TEXT;
BEGIN
	FOR rec in ( SELECT * FROM typeInheritance WHERE typeId=NEW.super_typeId ) LOOP
	   IF ( SELECT COUNT( * ) FROM typeInheritance WHERE typeId=rec.super_typeId AND super_typeId=NEW.typeId ) > 0 THEN
	      RAISE EXCEPTION 'Error: Can not create super type as subtype for its subtype.';
	   END IF;
	END LOOP;
	RETURN NEW;
	
END;
$tSubTypeSetChk$ LANGUAGE plpgsql;

CREATE TRIGGER tSubTypeSetChk BEFORE INSERT ON typeInheritance FOR EACH ROW EXECUTE PROCEDURE fSubTypeSetChk();







CREATE OR REPLACE FUNCTION fSubTypeChkVolume() RETURNS TRIGGER AS $tSubTypeChkVolume$
DECLARE
	len   INTEGER;
	wid   INTEGER;
	dep   INTEGER;
	vol   INTEGER;
	vol2  INTEGER;
BEGIN
	IF (SELECT COUNT(*) FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.super_typeID) = 0 THEN
	RAISE EXCEPTION 'Error: No such container exists in container table.';
	END IF;
	SELECT containerLengthAB, containerLengthAD, containerLengthAE INTO len, wid, dep FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.super_typeID;
	vol = len*wid*dep;

	IF ((SELECT COUNT(*) FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.typeId) > 0)
	THEN
		SELECT containerLengthAB, containerLengthAD, containerLengthAE INTO len, wid, dep FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.typeId;
		vol2 = len*wid*dep;
	ELSIF ((SELECT COUNT(*) FROM nonContainerConcreteTypes WHERE nonContainerConcreteTypeID=NEW.typeId) > 0)
	THEN
		SELECT nonContainerLengthAB, nonContainerLengthAD, nonContainerLengthAE INTO len, wid, dep FROM nonContainerConcreteTypes WHERE nonContainerConcreteTypeID=NEW.typeId;
		vol2 = len*wid*dep;
	ELSE
		RAISE EXCEPTION 'Error: Subtype does not exist.';
	END IF;


	IF (vol) <= vol2
	THEN
	RAISE EXCEPTION 'Error: No enough space in the super type container.';
	END IF;
	UPDATE containerConcreteTypes SET containerRemainingVol=vol-vol2 WHERE containerConcreteTypeID=NEW.super_typeId;
	RETURN NEW;
END;

$tSubTypeChkVolume$ LANGUAGE plpgsql;

CREATE TRIGGER tSubTypeChkVolume BEFORE INSERT ON typeInheritance FOR EACH ROW EXECUTE PROCEDURE fSubTypeChkVolume();




CREATE OR REPLACE FUNCTION fContainerRemainingVolume() RETURNS TRIGGER AS $tContainerRemainingVolume$
BEGIN
	NEW.containerRemainingVol = NEW.containerLengthAB * NEW.containerLengthAD * NEW.containerLengthAE;
	RETURN NEW;
END;
$tContainerRemainingVolume$ LANGUAGE plpgsql;


CREATE TRIGGER tContainerRemainingVolume BEFORE INSERT ON containerConcreteTypes FOR EACH ROW EXECUTE PROCEDURE fContainerRemainingVolume();



-- CREATE OR REPLACE FUNCTION fValidContainerChk() RETURNS TRIGGER AS $tValidContainerChk$
-- BEGIN
-- 	IF (SELECT COUNT(*) FROM nonContainerConcreteTypes WHERE nonContainerConcreteTypeID = NEW.containerConcreteTypeID) > 0 THEN
-- 	   RAISE EXCEPTION 'Error: NonContainer can not be a container.';
-- 	END IF;
-- 	RETURN NEW;
-- END;
-- $tValidContainerChk$ LANGUAGE plpgsql;

-- CREATE TRIGGER tValidContainerChk BEFORE INSERT ON containerConcreteTypes FOR EACH ROW EXECUTE PROCEDURE fValidContainerChk();


