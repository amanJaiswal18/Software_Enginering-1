create table types (
    type_id     text primary key,
    type_name   text not null,
    description text
);

create table typeInheritance (
    type_id       text references types(type_id),
    super_type_id text references types(type_id),
    primary key(type_id, super_type_id)
);

create table containerConcreteTypes (
    container_Concrete_Type_ID text primary key references types (type_id),
    container_length_AB       integer not null,
    container_length_AD       integer not null,
    container_length_AE       integer not null,
     container_thickness       integer not null,
     user_object_comment_global text
);

create table boxWalls (
    box_wall_side_name text primary key
);

create table containerConcreteTypeFaces (
    container_concrete_wall_id text primary key,
    container_Concrete_Type_ID text references containerConcreteTypes(container_Concrete_Type_ID),
    container_sides          text references boxWalls(box_wall_side_name),
    wall_orientation          text not null
);

create table containerConcreteTypeFaceDoors (
    container_concrete_door_id text primary key,
    container_concrete_wall_id text references containerConcreteTypeFaces(container_concrete_wall_id),
    door_width_AB              integer not null,
    door_length_AD             integer not null,
    door_displacement_AB      integer not null,
    door_displacement_AD      integer not null,
    door_alignment_AB         text not null,
    door_alignment_AD         text not null
);


create table objectTypes (
    object_type text primary key
    /* i.e. container or nonContainer */
);

create table objects (
    object_id   text primary key,
    object_type text references objectTypes(object_type)
    
);

create table physicalEntities (
    physical_entity_id text primary key,
    entity_name      text not null
);

create table propertyTypes (
    property_type     text primary key, /* only from a list - date, string, bool, number, list of string, bool, number*/
    physical_entity_id text references physicalEntities(physical_entity_id), 
    user_description  text
);



create table properties (
    property_id      text primary key,
    property_type    text references propertyTypes(property_type), -- references propertyTypes(property_type)
    user_description text
);

create table Unit_List (
    unit_id               text primary key,
    unit_name             text,
    unit_abbr             text,
    physical_entity_id      text references physicalEntities(physical_entity_id), 
    multiplication_factor float
);

create table measurementUnits (
       measurement_unit_id   text primary key,
       measurement_unit_type text references Unit_List(unit_id)
);

create table containerConcreteTypeProperties (
    container_Concrete_Type_ID text references containerConcreteTypes (container_Concrete_Type_ID),
    property_id              text references properties(property_id),
    property_value           text,
    measurementUnits        text references measurementUnits(measurement_unit_id),
    user_comment_global       text,
    primary key(container_Concrete_Type_ID, property_id)
);

create table containerObjects (
    object_id                text references objects(object_id),
    container_Concrete_Type_Id text references containerConcreteTypes (container_Concrete_Type_Id),
    user_label               text,
    user_object_comment_local  text,
    primary key(object_id)
);

create table nonContainerConcreteTypes (
    nonContainerConcreteTypeID text primary key references types (type_id),
    nonContainerLengthAB       integer not null,
    nonContainerLengthAD       integer not null,
    nonContainerLengthAE       integer not null,
     user_object_comment_global text
);

create table nonContainerObjects (
    object_id                   text references objects(object_id),
    nonContainerConcreteTypeId text references nonContainerConcreteTypes (nonContainerConcreteTypeId),
    user_label                  text,
    user_object_comment_local     text,
    primary key(object_id)
);

create table nonContainerObjectProperties (
    object_id         text references nonContainerObjects (object_id),
    user_assigned_id   text not null,
    property_id       text references properties(property_id), 
    property_value    text not null,
    user_comment_local text,
    user_label        text not null,
    primary key(object_id, property_id)
);

create table childrenList (
    parentObjectID         text references containerObjects(object_id),
    object_id               text references objects(object_id),
    displacement_AB         integer not null,
    displacement_AD         integer not null,
    displacement_AE         integer not null,
    child_front_facing_parent text    not null,
    child_top_facing_parent   text    not null
);

create table compulsoryProperties ( 
       property_id text references properties (property_id)
    /*If property is there it is compulsory else its not.*/
 );

create table immutableProperties ( 
    property_id text references properties (property_id)
    /*If property is there it is immutable else its not.*/
);

drop table typeinheritance;
drop table containerconcretetypefacedoors;
drop table containerconcretetypefaces;
drop table containerconcretetypeproperties;
drop table noncontainerobjectproperties;
drop table noncontainerobjects;
drop table noncontainerconcretetypes;
drop table immutableproperties;
drop table measurementunits;
drop table unit_list;
drop table compulsoryproperties;
drop table childrenlist;
drop table boxwalls;
drop table containerobjects;
drop table containerconcretetypes;
drop table types;
drop table properties;
drop table propertytypes;
drop table physicalentities;
drop table objects;
drop table objecttypes;

