insert into types values ('obj1', 'Object_1'), ('obj2', 'Object_2'), ('obj3', 'Object_3'), ('obj4', 'Object_4'), ('obj5', 'Object_5');

insert into containerConcreteTypes values ('obj1', 10, 10, 10, 1), ('obj2', 8, 8, 8, 1);

insert into nonContainerConcreteTypes values('obj3', 7, 7, 7, 1), ('obj4', 10, 10, 10, 1);

insert into typeInheritance values('obj3', 'obj1');

--This should throw error:
insert into typeInheritance values('obj4', 'obj1');

insert into wallTable values ('wall1',4,4) ,('wall2',3,4),('wall3',4,5),('wall4',4,6),('wall5',5,4),('wall6',4,3) ;

insert into concreteTypeFaceSet values ('set1','wall1','wall2','wall3','wall4','wall5','wall6');

insert into containerConcreteTypeFaces values('set1',);

