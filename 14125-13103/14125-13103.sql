/*
# of Tables: 24
Table List:
      1.  types 
      2.  typeInheritance
      3.  containerConcreteTypes
      4.  validOrientations
      5.  wallTable
      6.  concreteTypeFaceSet
      7.  containerConcreteTypeFaces
      8.  containerConcreteTypeFaceDoors
      9.  containerConcreteTypeProperties
      10. objects
      11. containerObjects
      12. containerObjectProperties
      13. nonContainerConcreteTypes
      14. nonContainerConcreteTypeFaces
      15. nonContainerObjects
      16. nonContainerObjectProperties
      17. childrenList
      18. propertyTypes
      19. properties
      20. physicalEntities
      21. unit_list
      22. measurementUnits
      23. compulsoryProperties    
      24. immutableProperties          


# of Validations: 10 
Validation List:
      1.  AlphaNumeric String for IDs
      2.  Unique Wall
      3.  Type Inheritance
      4.  Container/nonContainer specifications
      5.  Valid Wall sizes
      6.  Valid Door Sizes
      7.  Unique Doors
      8.  Valid Color Ranges
      9.  Valid height/widht/length
      10. Object inside Container (Via Volume)
      (Misc. Validations i.e. Not Null, Unique etc.)

# of Functions: 9
      1.  fAlphaNumericCheck
      	  Need: To test a string is Alpha Numeric or Not.
	  Input: String
	  Output: True (if alphaNumeric) false (otherwise)
	  
      2.  fValidContainerChk
      	  Need: To check if a concreteContainer type is not already defined as a concreteNonContainer or Vice Versa
	  Input: concreteTypeID String, ChceckBit (0 to check container is not already non container, vice versa otherwise)
	  Output: True (if not an error, false otherwise)

      3.  fValidWallChk
      	  Need: To check if a wall can fit on the container.
	  Input: wallId, containerId
	  Output: True if Valid, False Otherwise

      4.  fValidContainerFaceChk
      	  Need: Check if a wallSet is already used for nonContainer or vice versa for container.
	  Input: wallSetId, checkBit(0 to check if in nonContainer, 1 Otherwise)
	  Output: True if Not Used, False if Used

      5.  fValidDoorOnWallChk
      	  Need: To check if the door dimensions are valid according to the wall dimensions
	  input: doorId, wallId
	  Output: True if Valid, False if Invalid
	
      6.  fValidChildParentPair
      	  Need: To check if children is Not a parent of the defining parent.
	  Input: ChildrenId, ParentId
	  Output: True if Valid, False otherwise.

      7. fSubTypeSetChk
      	 Need: To check if the subtype is not already a supertype of it's supertype's sypertype.
	 Input: NULL
	 Output: Trigger if Valid, Exception raised if Invalid.

      8. fContainerRemainingVolume
      	 Need: To update the remaining volume column for the container after inserting an object
	 Input: NULL(Triggered)
	 Output: New Updated Value Trigger

      9. fSubTypeChkVolume()
      	 Need: To check if the object can or can not be placed inside the container.
	 Input: NULL(Triggered)
	 Output: New Trigger
*/


/* Drop Commands */
drop trigger tsubtypesetchk on typeinheritance;
drop table immutableproperties ;
drop table compulsoryproperties;
drop table childrenlist;
drop table noncontainerobjectproperties;
drop table containerobjectproperties;
drop table noncontainerobjects;
drop table containerconcretetypeproperties;
drop table containerobjects;
drop table measurementunits;
drop table unit_list;
drop table properties;
drop table propertytypes;
drop table physicalentities;
drop table objects;
drop table containerconcretetypefacedoors;
drop table containerconcretetypefaces;
drop table typeinheritance;
drop table containerconcretetypes;
drop table noncontainerconcretetypefaces;
drop table concretetypefaceset;
drop table walltable;
drop table noncontainerconcretetypes;
drop table validorientations;
drop table types;
drop function fvalidcontainerchk (varchar, integer);
drop function fAlphaNumericCheck (varchar);
drop function fValidWallChk (varchar,varchar);
drop function fValidContainerFaceChk (varchar,integer);
drop function fValidDoorOnWallChk (integer,integer,varchar);
drop function fValidChildParentPair(varchar,varchar);
drop function fSubTypeChkVolume();
/*Creation Commands*/
/* Function to check if a given string is Alpha Numeric or Not. */
CREATE OR REPLACE FUNCTION fAlphaNumericCheck( str VARCHAR( 100 ) ) RETURNS BOOLEAN AS $$
BEGIN
	RETURN (str ~ '^[A-Za-z]+[0-9a-zA-Z]*');
END;
$$ LANGUAGE plpgsql;



CREATE TABLE types (
    typeId      TEXT PRIMARY KEY,
    type_name   TEXT NOT NULL,
    description TEXT,
    /*
    Validation: typeId should be alphanumeric string only.
    CHECK constraint is required to determine if the input string matches 
    the valid regular expression for the alphanumeric string.
    */
    CHECK ( fAlphaNumericCheck( typeId ) )
);



/*
Validation: function to determine whether a container is already a noncontainer or Vice Verse.
Function takes two arguments: 1. string for new container/noncontainerId, 2. integer either 0 or nonZero
2.1 If chkBit is 0 then it will check if the container is nonContainer
2.2 If chkBit is 'nonZero' then it will check if the nonContainer is container.
*/
CREATE OR REPLACE FUNCTION fValidContainerChk( str varchar( 100 ), chkBit INTEGER ) RETURNS BOOLEAN AS $$
BEGIN
	IF ( chkBit ) = 0 THEN
	     /* if the count is greater than 0, it implies that container has already been defined for being a nonContainer*/
		IF ( SELECT COUNT( * ) FROM nonContainerConcreteTypes WHERE nonContainerConcreteTypeID = str ) > 0 THEN
		   RETURN FALSE;
		END IF;
		RETURN TRUE;
	END IF;
	/* if the count is greater than 0, it implies that nonContainer has already been defined as a container*/
	IF ( SELECT COUNT( * ) FROM containerConcreteTypes WHERE containerConcreteTypeID = str ) > 0 THEN
	   RETURN FALSE;
	END IF;
	RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE containerConcreteTypes (
    containerConcreteTypeID TEXT PRIMARY KEY REFERENCES types ( typeId ),
    /* Validation: Container length, width, depth and thickness should be greater than 0.*/
    containerLengthAB       INTEGER NOT NULL CHECK( containerLengthAB>0 ),
    containerLengthAD       INTEGER NOT NULL CHECK( containerLengthAD>0 ),
    containerLengthAE       INTEGER NOT NULL CHECK( containerLengthAE>0 ),
    containerThickness      INTEGER NOT NULL CHECK( containerThickness>0 ),
    /*
    Column containerRemainingVol Use: Store the remaining volume after putting a child inside the contianer.
    Note: Volume is being added automatically by the trigger, user doesn't need to put it themselves.
    */
    containerRemainingVol   INTEGER NOT NULL CHECK( containerRemainingVol >= 0 ),
    userObjectCommentGlobal TEXT,
    /* 
     Validation: If a container is not already a nonContainer.
     CHECK constraint to determine whether a container is nonContainer or not already. 
     fValidContainerChk with chkBit as '0' is Called to validate it.
     */
    CHECK( fValidContainerChk( containerConcreteTypeID, 0 ))
);

CREATE TABLE typeInheritance (
    /*
    Validation: typeId & super_typeId should be alphanumeric.
    Since the typeId is refering to types(typeId) there's no need to check whether it is alphanumeric or not
    */
    typeId       TEXT REFERENCES types( typeId ),
    super_typeId TEXT REFERENCES containerConcreteTypes( containerConcreteTypeID ),
    PRIMARY KEY( typeId, super_typeId ),
    /* 
    Validation: If type1 is not being defined as its own super type
    Check constraint to determine if typeId is not equal to super_typeId
    */
    CHECK ( typeId != super_typeId )
);


/* 
Use: Table is used to determine if the orientation is valid or not for any face/door.
Values in the table are already been added at the creation of tables 
*/
CREATE TABLE validOrientations (
    boxValidWallOrientations TEXT PRIMARY KEY /*It is one of the ABCD, BCDA, CDAB, DABC, DCBA, CBAD, BADC,  ADCB */
);


/*
Use: Store the information of all the walls defined.
Walls for every container's wall set are being reffered to wallTable(wallId)
*/
CREATE TABLE wallTable (
    wallID TEXT PRIMARY KEY,
    widAB  INTEGER NOT NULL CHECK( widAB > 0 ),
    lenAD  INTEGER NOT NULL CHECK( lenAD > 0 ),
    /*
    Validation: typeId should be alphanumeric string only.
    CHECK constraint is required to determine if the input string matches 
    the valid regular expression for the alphanumeric string.
    */
    CHECK ( fAlphaNumericCheck( wallID ) )
    
);


/*
Use: Create a face set for all six walls of the container/nonContainer and 
all the set is applied to the container/noncontainer
*/
CREATE TABLE concreteTypeFaceSet (
    concreteWallSetID TEXT     PRIMARY KEY,
    leftWallID                 TEXT REFERENCES wallTable( wallID ),
    rightWallID                TEXT REFERENCES wallTable( wallID ),
    topWallID                  TEXT REFERENCES wallTable( wallID ),
    botWallID                  TEXT REFERENCES wallTable( wallID ),
    frontWallID                TEXT REFERENCES wallTable( wallID ),
    backWallID                 TEXT REFERENCES wallTable( wallID ),


    /*
    Validation: No wall should be used more than once
    Unique key will take care of this problem in every column
    */
    
    UNIQUE ( leftWallID, rightWallID, topWallID, botWallID, frontWallID, backWallID ),

    /* 
    Validation: No wall should be used more than once for any container
    Check if each wall in the container is different
    */
    CHECK  ( leftWallID != rightWallID AND leftWallID != topWallID AND leftWallID != botWallID AND leftWallID != frontWallID AND leftWallID != backWallID ),
    CHECK  ( rightWallID != topWallID AND rightWallID != botWallID AND rightWallID != frontWallID AND rightWallID != backWallID ),
    CHECK  ( topWallID != botWallID AND topWallID != frontWallID AND topWallID != backWallID ),
    CHECK  ( botWallID != frontWallID AND botWallID != backWallID ),
    CHECK  ( frontWallID != backWallID ),

    /*
    Validation: concreteWallSetId should be alphanumerice
    Check if the concreteWallSetId matches the regex String.
    */
    CHECK ( fAlphaNumericCheck( concreteWallSetId ) )

);

CREATE OR REPLACE FUNCTION fValidWallChk( str VARCHAR( 100 ), containerId VARCHAR( 100 ) ) RETURNS BOOLEAN AS $$
DECLARE
	len INTEGER;
	wid INTEGER;
	conLen INTEGER;
	conWid INTEGER;
	conDep INTEGER;
	leftWall VARCHAR( 100 );
	topWall VARCHAR( 100 );
	frontWall VARCHAR( 100 );
	backWall VARCHAR( 100 );
	rightWall VARCHAR( 100 );
	botWall VARCHAR( 100 );
BEGIN

	   SELECT containerLenghtAB, containerLengthAD, containerLengthAE INTO conWid, conDep, conLen from containerConcreteTypeFaces WHERE containerConcreteTypeID = containerId;
	   
	   SELECT leftWallID, topWallID, frontWallID, rightWallID, backWallID, botWallID INTO leftWall, topWall, frontWall, rightWall, backWall, botWall FROM containerConcreteWallSet WHERE concreteWallSetID = str;
	   --Top Face Validation
	   SELECT lenAD, widAB INTO len, wid FROM wallTable WHERE wallID = topWall;
	   IF wid != conWid AND len != conDep THEN
	      RETURN FALSE;
	   END IF;
	    --left Face Validation
	   SELECT lenAD, widAB INTO len, wid FROM wallTable WHERE wallID = leftWall;
	   IF wid != conDep AND len != conLen THEN
	   RETURN FALSE;
	   END IF;
	    --front Face Validation
	   SELECT lenAD, widAB INTO len, wid FROM wallTable WHERE wallID = frontWall;
	   IF wid != conWid AND len != conLen THEN
	   RETURN FALSE;
	   END IF;
	    --Bottom Face Validation
	   SELECT lenAD, widAB INTO len, wid FROM wallTable WHERE wallID = botWall;
	   IF wid != conWid AND len != conDep THEN
	      RETURN FALSE;
	   END IF;
	    --Right Face Validation
	   SELECT lenAD, widAB INTO len, wid FROM wallTable WHERE wallID = rightWall;
	   IF wid != conDep AND len != conLen THEN
	   RETURN FALSE;
	   END IF;
	    --Back Face Validation
	   SELECT lenAD, widAB INTO len, wid FROM wallTable WHERE wallID = backWall;
	   IF wid != conWid AND len != conLen THEN
	   RETURN FALSE;
	   END IF;
	   
	   RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
		

CREATE OR REPLACE FUNCTION fValidContainerFaceChk( str varchar( 100 ), chkBit INTEGER ) RETURNS BOOLEAN AS $$
DECLARE
	res BOOLEAN;
BEGIN
	IF ( chkBit ) = 0 then
		IF ( SELECT COUNT( * ) FROM nonContainerConcreteTypeFaces WHERE nonContainerConcreteWallSetID = str ) > 0 THEN
		   RETURN FALSE;
		END IF;		
		RETURN TRUE;
	END IF;
	IF ( SELECT COUNT( * ) FROM containerConcreteTypeFaces WHERE containerConcreteWallSetID = str ) > 0 THEN
	   RETURN FALSE;
	END IF;
	RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE containerConcreteTypeFaces (
    containerConcreteWallSetID TEXT REFERENCES concreteTypeFaceSet( concreteWallSetID ),
    containerConcreteTypeID    TEXT REFERENCES types( typeId ),
    wallOrientation            TEXT REFERENCES validOrientations( boxValidWallOrientations ),
    colorRed                   INTEGER CHECK ( colorRed >= 0 AND colorRed <= 255 ),
    colorGreen                 INTEGER CHECK ( colorGreen >= 0  AND colorGreen <= 255 ),
    colorBlue                  INTEGER CHECK ( colorBlue >= 0 AND colorBlue <= 255 ),
    PRIMARY KEY( containerConcreteWallSetID, containerConcreteTypeID ),
    CHECK( fValidContainerFaceChk( containerConcreteWallSetID, 0 ) ),
    CHECK( fValidWallChk( containerConcreteWallSetID, containerConcreteTypeID ) )
--    CHECK( fValidWallSet( containerConcreteWallSetID, 0 ) )
    /*one Wall can not belong to other object or the same object for the another wall*/
);

CREATE OR REPLACE FUNCTION fValidDoorOnWallChk( doorLen INTEGER, doorWid INTEGER, conWallID VARCHAR( 100 ) ) RETURNS BOOLEAN AS $$
DECLARE
	len INTEGER;
	wid INTEGER;
BEGIN
	SELECT widAB, lenAD INTO len, wid FROM wallTable WHERE wallId=conWallID;
	IF len<=doorLen OR wid<=doorWid THEN
	   RETURN FALSE;
	END IF;
	RETURN TRUE;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE containerConcreteTypeFaceDoors (
    containerConcreteDoorID TEXT PRIMARY KEY,
    containerConcreteWallID TEXT REFERENCES wallTable( wallID ),
    doorWidthAB             INTEGER NOT NULL,
    doorLengthAD            INTEGER NOT NULL,
    doorDisplacementAB      INTEGER NOT NULL,
    doorDisplacementAD      INTEGER NOT NULL,
    doorAlignment           TEXT NOT NULL REFERENCES validOrientations( boxValidWallOrientations ),
    CHECK ( fAlphaNumericCheck( containerConcreteDoorId ) ),
    CHECK ( fValidDoorOnWallChk( doorLengthAD, doorWidthAB, containerConcreteWallID ) )
    
);


CREATE TABLE objects (
    objectID   TEXT PRIMARY KEY,
    objectType TEXT REFERENCES types( typeId ),
    /*
    Validation: objectID should be alphanumerice
    Check if the objectID matches the regex String.
    */
    CHECK ( fAlphaNumericCheck( objectID ) )
);

CREATE TABLE physicalEntities (
    physicalEntityID TEXT PRIMARY KEY,
    entity_name      TEXT NOT NULL,
    CHECK ( fAlphaNumericCheck( physicalEntityID ) )
    
);

CREATE TABLE propertyTypes (
    propertyType     TEXT PRIMARY KEY, /* only from a list - date, string, bool, number, list of string, bool, number*/
    physicalEntityID TEXT REFERENCES physicalEntities( physicalEntityID ), 
    userDescription  TEXT,
    CHECK ( fAlphaNumericCheck( propertyType ) )
);


CREATE TABLE properties (
    propertyID      TEXT PRIMARY KEY,
    propertyType    TEXT REFERENCES propertyTypes( propertyType ),
    userDescription TEXT,
    CHECK ( fAlphaNumericCheck( propertyID ) )
);

CREATE TABLE Unit_List (
    unit_id               TEXT PRIMARY KEY,
    unit_name             TEXT NOT NULL,
    unit_abbr             TEXT NOT NULL,
    physicalEntityID      TEXT REFERENCES physicalEntities( physicalEntityID ), 
    multiplication_factor FLOAT NOT NULL DEFAULT 1,
    CHECK ( fAlphaNumericCheck( unit_id ) )
);

CREATE TABLE measurementUnits (
    measurementUnitId   TEXT PRIMARY KEY,
    measurementUnitType TEXT REFERENCES Unit_List( unit_id ),
    CHECK ( fAlphaNumericCheck( measurementUnitId ) )
);

CREATE TABLE containerConcreteTypeProperties (
    containerConcreteTypeID TEXT REFERENCES containerConcreteTypes (containerConcreteTypeID),
    propertyID              TEXT REFERENCES properties(propertyID),
    propertyValue           TEXT,
    measurementUnits        TEXT REFERENCES measurementUnits(measurementUnitID),
    userCommentGlobal       TEXT,
    PRIMARY KEY( containerConcreteTypeID, propertyID )
);

CREATE TABLE containerObjects (
    objectID                TEXT REFERENCES objects( objectID ),
    containerConcreteTypeId TEXT REFERENCES containerConcreteTypes ( containerConcreteTypeId ),
    userLabel               TEXT,
    userObjectCommentLocal  TEXT,
    PRIMARY KEY( objectID )
);

CREATE TABLE ContainerObjectProperties (
    objectID         TEXT REFERENCES containerObjects ( objectID ),
    userAssignedID   TEXT NOT NULL,
    propertyID       TEXT REFERENCES properties( propertyID ), 
    propertyValue    TEXT NOT NULL,
    userCommentLocal TEXT,
    userLabel        TEXT NOT NULL,
    PRIMARY KEY( objectID, propertyID )
);

CREATE TABLE nonContainerConcreteTypes (
    nonContainerConcreteTypeID TEXT PRIMARY KEY REFERENCES types( typeId ),
    nonContainerLengthAB       INTEGER NOT NULL,
    nonContainerLengthAD       INTEGER NOT NULL,
    nonContainerLengthAE       INTEGER NOT NULL,
    userObjectCommentGlobal    TEXT,
    CHECK( fValidContainerChk( nonContainerConcreteTypeID, 1 ) )	
);

CREATE TABLE nonContainerConcreteTypeFaces (
    nonContainerConcreteWallSetID TEXT REFERENCES concreteTypeFaceSet( concreteWallSetID ),
    nonContainerConcreteTypeID    TEXT REFERENCES nonContainerConcreteTypes( nonContainerConcreteTypeID ),
    wallOrientation               TEXT REFERENCES validOrientations( boxValidWallOrientations ),
    PRIMARY KEY( nonContainerConcreteWallSetID, nonContainerConcreteTypeID ),
    CHECK( fValidContainerFaceChk( nonContainerConcreteWallSetID, 0 ) )
);

CREATE TABLE nonContainerObjects (
    objectID                   TEXT REFERENCES objects( objectID ),
    nonContainerConcreteTypeId TEXT REFERENCES nonContainerConcreteTypes ( nonContainerConcreteTypeId ),
    userLabel                  TEXT,
    userObjectCommentLocal     TEXT,
    PRIMARY KEY( objectID )
);

CREATE TABLE nonContainerObjectProperties (
    objectID         TEXT REFERENCES nonContainerObjects ( objectID ),
    userAssignedID   TEXT NOT NULL,
    propertyID       TEXT REFERENCES properties( propertyID ), 
    propertyValue    TEXT NOT NULL,
    userCommentLocal TEXT,
    userLabel        TEXT NOT NULL,
    PRIMARY KEY( objectID, propertyID )
);


--Check if the child is not already the parent of the same child.
CREATE OR REPLACE FUNCTION fValidChildParentPair( str varchar( 100 ), str1 varchar( 100 ) ) RETURNS BOOLEAN AS $$
BEGIN
	IF ( SELECT COUNT( * ) FROM childrenList WHERE objectID = str AND parentObjectID = str1 ) > 0 THEN
	   RETURN FALSE;
	END IF;
	RETURN TRUE;
END;
$$ LANGUAGE plpgsql;


CREATE TABLE childrenList (
    parentObjectID         TEXT REFERENCES containerObjects( objectID ),
    objectID               TEXT REFERENCES objects( objectID ),
    displacementAB         INTEGER NOT NULL,
    displacementAD         INTEGER NOT NULL,
    displacementAE         INTEGER NOT NULL,
    childFrontFacingParent TEXT    NOT NULL,
    childTopFacingParent   TEXT    NOT NULL
    CHECK( objectID != parentObjectID )
    CHECK( fValidChildParentPair( parentObjectID, objectID ) )
);

CREATE TABLE compulsoryProperties ( 
    propertyID TEXT REFERENCES properties( propertyID )
    /*If property is there it is compulsory else its not.*/
);

CREATE TABLE immutableProperties ( 
    propertyID TEXT REFERENCES properties ( propertyID )
    /*If property is there it is immutable else its not.*/
);

CREATE OR REPLACE FUNCTION fSubTypeSetChk() RETURNS TRIGGER AS $tSubTypeSetChk$
DECLARE
	rec record;
	sTypeId TEXT;
BEGIN
	FOR rec in ( SELECT * FROM typeInheritance WHERE typeId=NEW.super_typeId ) LOOP
	   IF ( SELECT COUNT( * ) FROM typeInheritance WHERE typeId=rec.super_typeId AND super_typeId=NEW.typeId ) > 0 THEN
	      RAISE EXCEPTION 'Error: Can not create super type as subtype for its subtype.';
	   END IF;
	END LOOP;
	RETURN NEW;
	
END;
$tSubTypeSetChk$ LANGUAGE plpgsql;

CREATE TRIGGER tSubTypeSetChk BEFORE INSERT ON typeInheritance FOR EACH ROW EXECUTE PROCEDURE fSubTypeSetChk();







CREATE OR REPLACE FUNCTION fSubTypeChkVolume() RETURNS TRIGGER AS $tSubTypeChkVolume$
DECLARE
	len   INTEGER;
	wid   INTEGER;
	dep   INTEGER;
	vol   INTEGER;
	vol2  INTEGER;
BEGIN
	IF (SELECT COUNT(*) FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.super_typeID) = 0 THEN
	RAISE EXCEPTION 'Error: No such container exists in container table.';
	END IF;
	SELECT containerLengthAB, containerLengthAD, containerLengthAE INTO len, wid, dep FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.super_typeID;
	vol = len*wid*dep;

	IF ((SELECT COUNT(*) FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.typeId) > 0)
	THEN
		SELECT containerLengthAB, containerLengthAD, containerLengthAE INTO len, wid, dep FROM containerConcreteTypes WHERE containerConcreteTypeID=NEW.typeId;
		vol2 = len*wid*dep;
	ELSIF ((SELECT COUNT(*) FROM nonContainerConcreteTypes WHERE nonContainerConcreteTypeID=NEW.typeId) > 0)
	THEN
		SELECT nonContainerLengthAB, nonContainerLengthAD, nonContainerLengthAE INTO len, wid, dep FROM nonContainerConcreteTypes WHERE nonContainerConcreteTypeID=NEW.typeId;
		vol2 = len*wid*dep;
	ELSE
		RAISE EXCEPTION 'Error: Subtype does not exist.';
	END IF;


	IF (vol) <= vol2
	THEN
	RAISE EXCEPTION 'Error: No enough space in the super type container.';
	END IF;
	UPDATE containerConcreteTypes SET containerRemainingVol=vol-vol2 WHERE containerConcreteTypeID=NEW.super_typeId;
	RETURN NEW;
END;

$tSubTypeChkVolume$ LANGUAGE plpgsql;

CREATE TRIGGER tSubTypeChkVolume BEFORE INSERT ON typeInheritance FOR EACH ROW EXECUTE PROCEDURE fSubTypeChkVolume();




CREATE OR REPLACE FUNCTION fContainerRemainingVolume() RETURNS TRIGGER AS $tContainerRemainingVolume$
BEGIN
	NEW.containerRemainingVol = NEW.containerLengthAB * NEW.containerLengthAD * NEW.containerLengthAE;
	RETURN NEW;
END;
$tContainerRemainingVolume$ LANGUAGE plpgsql;


CREATE TRIGGER tContainerRemainingVolume BEFORE INSERT ON containerConcreteTypes FOR EACH ROW EXECUTE PROCEDURE fContainerRemainingVolume();


/*Insertion into table for validations*/
INSERT INTO validOrientations VALUES( 'ABCD' ), ( 'BCDA' ), ( 'CDAB' ), ( 'DABC' ), ( 'DCBA' ), ( 'CBAD' ), ( 'BADC' ), ( 'ADCB' );

