create table types(
    typeId text primary key,
    typeName text not null,
    description text
);

create table typeInheritance (
    typeId text references types(typeId),
    superTypeId text references types(typeId),
    primary key(typeId)
);

create table containerConcreteTypes (
    containerConcreteTypeId text primary key references types(typeId),
    containerLengthAB integer not null, 
    containerLengthAD integer not null, 
    containerLengthAE integer not null, 
    containerThickness integer not null,
    userObjectCommentGlobal text
);

create table boxWalls(
	boxWallSideName text primary key
);

create table containerConcreteTypeFaces (
      containerConcreteWallId text primary key,
      containetConcreteTypeId text references containerConcreteTypes(containerConcreteTypeId),
      containerSides text references boxWalls(boxWallSideName),
      WallOrientation text not null
);


create table objects(
       objectID text primary key,
       objectName text,
       typeId text references types(typeId)
);


create table nonContainerConcreteTypes (
    nonContainerConcreteTypeId text primary key references types(typeId),
    nonContainerLengthAB integer not null, 
    nonContainerLengthAD integer not null, 
    nonContainerLengthAE integer not null, 
    userObjectCommentGlobal text
);

create table nonContainerConcreteTypeFaces (
	
    nonContainerConcreteTypeId text primary key references nonContainerConcreteTypes (nonContainerConcreteTypeId),
      roofID text not null,
      roofName text,
      roofColor integer,
      rightID text not null,
      rightName text,
      rightColor integer,
      leftID text not null,
      leftName text,
      leftColor integer,
      floarID text not null,
      floarName text,
      floarColor integer,
      frontID text not null,
      frontName text,
      frontColor integer,
      backID text not null,
      backName text,
      backColor integer
);

create table physicalEntities(
    physicalEntityID text primary key,
    entityName text
);

create table propertytypes (
    propertyType text primary key, /* only from a list - date, string, bool, number, list of string, bool, number*/
    physicalEntityID text references physicalEntities(physicalEntityID), 
    userDescription text
);

create table properties (
    propertyID text primary key, 
    propertyType text references propertytypes(propertyType),
    userDescription text
);

create table UnitList (
     unitId text primary key,
     unitName text,
     unitAbbr text,
     physicalEntityID text references physicalEntities(physicalEntityID), 
     multiplicationFactor float
 );

create table measurementUnits(
	measurementUnitID text primary key,
	measurementUnitType text references UnitList(unitId)
);

create table containerConcreteTypeProperties (
    containerConcreteTypeId text primary key references containerConcreteTypes (containerConcreteTypeId),
    propertyID text references properties(propertyID), 
    propertyValue text, /* this will be the default property, can be overridden by making entry in objectProperties */
    measurementUnitID text references measurementUnits(measurementUnitID),
   userCommentGlobal text 
);




create table containerObjects (
     objectID text primary key,
     containerConcreteTypeId text references containerConcreteTypes (containerConcreteTypeId),
     userLabel text,
     userObjectCommentLocal text
 );

create table nonContainerObjects (
     objectID text primary key,
     nonContainerConcreteTypeId text references nonContainerConcreteTypes (nonContainerConcreteTypeId),
     userLabel text,
     userObjectCommentLocal text
 );

create table childObjectList(
        objectID text references containerObjects (objectID),
        childObjectID text primary key, 
        displacementAB integer not null,
        displacementAE integer not null,
        displacementAD integer not null
 );

create table nonContainerObjectProperties (
     objectID text primary key references objects (objectID),
     userAssignedID text NOT NULL,
     propertyID text references properties(propertyID), 
     propertyValue text NOT NULL,
     userCommentLocal text,
     userLabel text NOT NULL
     
 );

create table containerTypeWallsDoor(
        doorID text not null,
        wallID text not null,
        containerConcreteTypeId text primary key references containerConcreteTypes (containerConcreteTypeId),
        displacementAB integer not null,
        dispalcementAD integer not null,
        allignmentAB integer not null,
        dimAD integer not null,
        dimAB integer not null,
        allignmentAD text not null,
        allignementAB text not null,
        color integer not null
 );

create table compulsoryProperties ( 
	cumpulsory text primary key,
        propertie text,
        objectID text references objects (objectID)
 );

create table immutableProperties ( 
         objectID text primary key references objects (objectID)	
 );

 
 drop  table typeinheritance ;
 drop table noncontainerobjects ;
 drop table noncontainerobjectproperties ;
 drop table noncontainerconcretetypefaces ;
 drop table noncontainerconcretetypes ;
 drop table containerconcretetypefaces ;
 drop table containerconcretetypeproperties ;
 drop table containertypewallsdoor ;
 drop table properties ;
 drop table propertytypes ;
 drop table childobjectlist ;
 drop table immutableproperties ;
 drop table containerobjects ;
 drop table containerconcretetypes ;
 drop table compulsoryproperties ;
 drop table boxwalls ;
 drop table measurementunits ;
 drop table objects ;
 drop table types ;
 drop  table UnitList ;
 drop table physicalentities ;
 
