
def fAddBodyAttrJs (sBodyId):
   sAttrJs = "$(document.body).attr('id',  '" + sBodyId +"');\n"
   return sAttrJs



#Add attributes to an HTML Object using it's objID
def fAddAttributeJs (objId, objAttribute):
   sAttrJs = ""
   sAttrJs += "$('#" + objId + "').attr('" + objAttribute[0] + "', '" + objAttribute[1] + "');\n"
   return sAttrJs



#Create HTML Object using JS
def fGenerateJSObj (objType, objAttributes, objParentId, objId):
   sObjHTMLJs = "$(document.createElement('"+objType+"')).attr('id', '" + objId + "')"
   sObjHTMLJs = "$('#" + objParentId + "').append(" + sObjHTMLJs + ");\n"
   for item in objAttributes:
      sObjHTMLJs += str(fAddAttributeJs (objId, (item[0], item[1])))
   return sObjHTMLJs


def fGenerateJSHTML (objId, sHTML):
   sObjHTML = "$('#" + objId + "').after().html('" + sHTML + "');\n"
   return sObjHTML


def fGenerateJSEventIfs (eventCodeTuple):
   sEventIfs = ""
   nLen = len(eventCodeTuple)
   sEventIfs += "if (event.which == "
   if nLen > 3:
      for i in range (1, nLen - 3):
         sEventIfs += str(eventCodeTuple[i]) + " " + str(eventCodeTuple[nLen - 2]) + " event.which == "
      sEventIfs += str(eventCodeTuple[nLen-3]) + ") {\n"
      sEventIfs += eventCodeTuple[nLen-1] + "\n}\n"
   else:
      sEventIfs += str(eventCodeTuple[1]) + " ) {\n"
      sEventIfs += eventCodeTuple[nLen-1] + "\n}\n"
   return sEventIfs


# objId as String, eventCodeTuple i s tuple of >=2 elements
# in which first indicates the event code
# and last indicates the function's definition
def fGenerateJSEventHandler (objId, eventCodeTuple, isTag, sNewTag):
   sMultiEventCases = ""
   if isTag == True:
       sObjEventJS = "$('"
   else:
       sObjEventJS = "$('#"
   if len(eventCodeTuple) > 2:
      sMultiEventCases += fGenerateJSEventIfs(eventCodeTuple)
      sObjEventJS += objId + "').on('" + eventCodeTuple[0] + "', '" + sNewTag + "', " + "function (event) {\n" + sMultiEventCases + "\n});\n"
   else:
      sObjEventJS += objId + "').on('" + eventCodeTuple[0] + "', '" + sNewTag + "', "+ "function (event) {\n" + eventCodeTuple[1] + "\n});\n"
   return sObjEventJS

def fApplyAttributeTag (tagName, attributeTuple):
   sTagAttr = "$('" + tagName + "').attr('" + attributeTuple[0] + "', '" + attributeTuple[1] + "');\n"
   return sTagAttr
