Do not send response to this text before you read the entire text carefully.

Instructions:

1. You have to send a single file with extension "tar.gz" as attachment. No other extension will be processed further. Name of the attached file should be your roll number. Your roll number is a five digit string. If you do NOT name the file as per this rule, your assignment might be rejected.

2. You have to send an email to "assignments<DOT>pucsd<AT>gmail<DOT>com" with the subject:
PPSPC16_02_LISTDIR_SPC

Note that I am going to filter messages on the basis of the subject text ONLY. So, you should send the email with the exact subject text given above. Even slight change might result in rejection.
3. Deadline: 25/01/2016 10AM

If you miss the deadline your assignment will be rejected. If you don't know the current time you may use this link [IST] to find the current time.

[IST] http://wwp.greenwichmeantime.com/time-zone/asia/india/time/

The assignment problem text starts here.

You may fetch a very large negative credit (including potential disqualification from the entire course also) if you submit an assignment without checking it yourself that it works as expected for the conditions given in the assignment text. Read the assignment text very carefully before you submit the assignment solution. Submitting junk code that obviously doesn't work as expected in the name of assignment work will be dealt with in a very strict manner. Do not submit the program, if it does NOT conform the given specifications.

The specifications for expected behavior are given towards the end below.

Write a set of programs that will accomplish the following -

   program0 - supervisor ::
            - will launch the programs (program1, program2, program3) on user request 
            - will keep on running and monitoring the status of the programs program1,
              program2, program3
              and if it finds that a program is not running,
                  then it will start it
            - will terminate the monitored programs on user request or upon detection of
              "job finished" condition

   program1 - dirLister :: dirs2process.lst -> new-files-found.lst
            - list a directory, whose name is found in the file dirs2process.lst,
              and appends that list to new-files-found.lst

   program2 - filesProcessor :: new-files-found.lst -> (tmp-files-info.lst, files.output)
              appends the list of directories to a file named tmp-files-info.lst
              appends the list of non-directory files to a file named files.output

   program3 - dirSelector :: (new-files-found.lst, tmp-files-info.lst) -> (dirs2process.lst, dirs.output)
            - selects directories from the files (new-files-found.lst, tmp-files-info.lst)
              based on the given criteria (depth and symlinks)
              and
              appends them to the file named dirs2process.lst 
              and also to a file named dirs.output

              This program must handle symlinks correctly.

The specifications for expected behavior and for programming:
 - You may assume that program0 will not be terminated during the execution of the tasks.
 - You must NOT assume any of the programs (program1, program2, program3) to not fail 
   during their execution (note well that failures can be of various types (recall your 
   knowledge of OS; anyway I'll discuss this topic in detail in the next lecture),
   and you should handle as many of them as you can)
 - User may interact directly only with the supervisor program for I/O
   The user may launch the listing tasks by executing the program
   $ ./supervisor <depth> <path-to-dirs2process.lst>
 - With the above assumptions, once launched, the supervisor program should return the 
   results to the user upon finishing the listing task.
 - The names of the programs must be supervisor, dirLister, filesProcessor and dirSelector.
 - The programs must be written in the C programming language only. You should write
   Makefile to compile/build your programs.
 - Read the note-about-credit, note-about-tar-gz-archive and note-about-junk-code at [0].

 [0] http://tinyurl.com/2016-jan-assignments

The assignment problem text ends here.

If you have any doubts, you may meet me.

Do not send your assignment by replying to this mail without setting the subject text properly. The subject text to be used for this assignment is:
 PPSPC16_02_LISTDIR_SPC
